<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>

	<h3>Divisible of Five</h3>
	<?php whileLoop(); ?>

	<h3>Array Manipulation</h3>
	<?php array_push($students, 'JC') ?>
	<p><?php var_dump($students)?></p>
	<p><?= count($students); ?></p>

	<?php array_push($students, 'CJ') ?>
	<p><?php var_dump($students)?></p>
	<p><?= count($students); ?></p>

	<?php array_shift($students) ?>
	<p><?php var_dump($students)?></p>
	<p><?= count($students); ?></p>
	
</body>
</html>